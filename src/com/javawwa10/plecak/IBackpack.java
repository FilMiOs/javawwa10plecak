package com.javawwa10.plecak;

import java.util.List;

public interface IBackpack {

    public List<Item> pack(int volumeOfBackpack, List<Item> items);

}
