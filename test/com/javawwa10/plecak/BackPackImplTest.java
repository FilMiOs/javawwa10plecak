package com.javawwa10.plecak;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class BackPackImplTest {

    int volume;
    List<Item> items;

    @Before
    public void setup() {

        volume = 44;
        items = new ArrayList<>() {{
            add(new Item(4, 1));
            add(new Item(40, 5));
            add(new Item(22, 3));
            add(new Item(35, 12));
            add(new Item(100, 25));
            add(new Item(500, 2));
            add(new Item(15, 30));
        }};

    }

    @Test
    public void testBackpackWeight() {
        items.sort((a, b) -> a.getWeight() - b.getWeight());
        BackPackImplWeight backPackImplWeight = new BackPackImplWeight();
        backPackImplWeight.pack(volume,items);
    }

    @Test
    public void testBackpackValue() {
        items.sort((a, b) -> a.getValue() - b.getValue());
        BackPackImplValue backPackImplValue = new BackPackImplValue();
        backPackImplValue.pack(volume,items);
    }
}
